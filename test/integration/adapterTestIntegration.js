/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const mocha = require('mocha');
const path = require('path');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const util = require('util');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-salesforce_tooling',
      type: 'SalesforceTooling',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const SalesforceTooling = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Salesforce_tooling Adapter Test', () => {
  describe('SalesforceTooling Class Tests', () => {
    const a = new SalesforceTooling(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const userInfoPostChangeOwnPasswordBodyParam = {
      changeOwnPassword: {}
    };
    describe('#postChangeOwnPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChangeOwnPassword(userInfoPostChangeOwnPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.changeOwnPasswordResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postChangeOwnPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostDescribeSoqlListViewsBodyParam = {
      describeSoqlListViews: {}
    };
    describe('#postDescribeSoqlListViews - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSoqlListViews(userInfoPostDescribeSoqlListViewsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSoqlListViewsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postDescribeSoqlListViews', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostGetUserInfoBodyParam = {
      getUserInfo: {}
    };
    describe('#postGetUserInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetUserInfo(userInfoPostGetUserInfoBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getUserInfoResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postGetUserInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userInfoPostSetPasswordBodyParam = {
      setPassword: {}
    };
    describe('#postSetPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSetPassword(userInfoPostSetPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.setPasswordResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserInfo', 'postSetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostCreateBodyParam = {
      create: {}
    };
    describe('#postCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCreate(sObjectPostCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.createResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDeleteBodyParam = {
      delete: {}
    };
    describe('#postDelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDelete(sObjectPostDeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.deleteResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeLayoutBodyParam = {
      describeLayout: {}
    };
    describe('#postDescribeLayout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeLayout(sObjectPostDescribeLayoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeLayoutResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeLayout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeSObjectBodyParam = {
      describeSObject: {}
    };
    describe('#postDescribeSObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSObject(sObjectPostDescribeSObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSObjectResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeSObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostDescribeSObjectsBodyParam = {
      describeSObjects: {}
    };
    describe('#postDescribeSObjects - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeSObjects(sObjectPostDescribeSObjectsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeSObjectsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postDescribeSObjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostGetDeletedBodyParam = {
      getDeleted: {}
    };
    describe('#postGetDeleted - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetDeleted(sObjectPostGetDeletedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getDeletedResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postGetDeleted', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostGetUpdatedBodyParam = {
      getUpdated: {}
    };
    describe('#postGetUpdated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetUpdated(sObjectPostGetUpdatedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getUpdatedResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postGetUpdated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostRetrieveBodyParam = {
      retrieve: {}
    };
    describe('#postRetrieve - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRetrieve(sObjectPostRetrieveBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.retrieveResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postRetrieve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostSearchBodyParam = {
      search: {}
    };
    describe('#postSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSearch(sObjectPostSearchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.searchResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostUpdateBodyParam = {
      update: {}
    };
    describe('#postUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUpdate(sObjectPostUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.updateResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectPostUpsertBodyParam = {
      upsert: {}
    };
    describe('#postUpsert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUpsert(sObjectPostUpsertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.upsertResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'postUpsert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const globalPostDescribeGlobalBodyParam = {
      describeGlobal: {}
    };
    describe('#postDescribeGlobal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeGlobal(globalPostDescribeGlobalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeGlobalResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Global', 'postDescribeGlobal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const valuePostDescribeValueTypeBodyParam = {
      describeValueType: {}
    };
    describe('#postDescribeValueType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDescribeValueType(valuePostDescribeValueTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-salesforce_tooling-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Value', 'postDescribeValueType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowPostDescribeWorkitemActionsBodyParam = {
      describeWorkitemActions: {}
    };
    describe('#postDescribeWorkitemActions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDescribeWorkitemActions(workflowPostDescribeWorkitemActionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.describeWorkitemActionsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Workflow', 'postDescribeWorkitemActions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const executePostExecuteAnonymousBodyParam = {
      executeAnonymous: {}
    };
    describe('#postExecuteAnonymous - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExecuteAnonymous(executePostExecuteAnonymousBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.executeAnonymousResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Execute', 'postExecuteAnonymous', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const executePostRunTestsBodyParam = {
      runTests: {}
    };
    describe('#postRunTests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRunTests(executePostRunTestsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.runTestsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Execute', 'postRunTests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const executePostRunTestsAsynchronousBodyParam = {
      runTestsAsynchronous: {}
    };
    describe('#postRunTestsAsynchronous - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRunTestsAsynchronous(executePostRunTestsAsynchronousBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.runTestsAsynchronousResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Execute', 'postRunTestsAsynchronous', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostGetServerTimestampBodyParam = {
      getServerTimestamp: {}
    };
    describe('#postGetServerTimestamp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGetServerTimestamp(soapBindingPostGetServerTimestampBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.getServerTimestampResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postGetServerTimestamp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostInvalidateSessionsBodyParam = {
      invalidateSessions: {}
    };
    describe('#postInvalidateSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postInvalidateSessions(soapBindingPostInvalidateSessionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.invalidateSessionsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postInvalidateSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostLoginBodyParam = {
      login: {}
    };
    describe('#postLogin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLogin(soapBindingPostLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.loginResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const soapBindingPostLogoutBodyParam = {
      logout: {}
    };
    describe('#postLogout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLogout(soapBindingPostLogoutBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.logoutResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SoapBinding', 'postLogout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queryPostQueryBodyParam = {
      query: {}
    };
    describe('#postQuery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postQuery(queryPostQueryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queryResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Query', 'postQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queryPostQueryAllBodyParam = {
      queryAll: {}
    };
    describe('#postQueryAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postQueryAll(queryPostQueryAllBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queryAllResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Query', 'postQueryAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const queryPostQueryMoreBodyParam = {
      queryMore: {}
    };
    describe('#postQueryMore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postQueryMore(queryPostQueryMoreBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.queryMoreResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Query', 'postQueryMore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const querygetSOQLQueryParam = 'SELECT+FIELDS(ALL)+FROM+Task+LIMIT+20';
    describe('#getSOQLQuery - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSOQLQuery(querygetSOQLQueryParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('number', typeof data.response.totalSize);
                assert.equal('boolean', typeof data.response.done);
                assert.equal(true, Array.isArray(data.response.records));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SOQL', 'getSOQLQuery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sObjectName = 'fakedata';
    const sObjectId = 'fakedata';
    describe('#getSObjectDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSObjectDetails(sObjectName, sObjectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-salesforce_tooling-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'getSObjectDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const patchSObjecteBodyParam = {};
    describe('#patchSObject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSObject(sObjectName, sObjectId, patchSObjecteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-salesforce_tooling-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SObject', 'patchSObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
